// Obtener todos los cajones de estacionamiento
var parkingSpots = document.getElementsByClassName("parking-spot");

// Crear variables para indicar si cada cajón está ocupado y guardar los identificadores de los intervalos
var isOccupied = [];
var intervalIds = [];

// Recorrer cada cajón de estacionamiento
for (var i = 0; i < parkingSpots.length; i++) {
  // Establecer que el cajón no está ocupado y que no tiene un identificador de intervalo
  isOccupied[i] = false;
  intervalIds[i] = null;

  // Añadir un evento de clic a cada cajón de estacionamiento
  parkingSpots[i].addEventListener("click", function () {
    // Obtener el índice del cajón que se ha hecho clic
    var spotIndex = Array.prototype.indexOf.call(parkingSpots, this);

    // Verificar si el temporizador está activo
    if (isOccupied[spotIndex]) {
      // Detener el temporizador
      clearInterval(intervalIds[spotIndex]);
      intervalIds[spotIndex] = null;
      var timerElement = this.querySelector(".timer");
      if (timerElement != null) {
        this.removeChild(timerElement);
      }

      // Marcar el cajón como desocupado
      this.classList.remove("selected");
      isOccupied[spotIndex] = false;
    } else {
      // Seleccionar el cajón de estacionamiento
      this.classList.add("selected");

      // Crear un elemento de temporizador y añadirlo al cajón
      var timerElement = document.createElement("div");
      timerElement.className = "timer";
      timerElement.innerHTML = "00:00:00";
      this.appendChild(timerElement);

      // Empezar a contar el tiempo
      var startTime = new Date().getTime();

      // Función para actualizar el tiempo restante y el precio
      function updateTime() {
        var currentTime = new Date().getTime();
        var elapsedTime = currentTime - startTime;
        var elapsedHours = Math.floor(elapsedTime / 3600000);
        var elapsedMinutes = Math.floor((elapsedTime % 3600000) / 60000);
        var elapsedSeconds = Math.floor((elapsedTime % 60000) / 1000);

        // Actualizar el temporizador
        timerElement.innerHTML =
          ("0" + elapsedHours).slice(-2) +
          ":" +
          ("0" + elapsedMinutes).slice(-2) +
          ":" +
          ("0" + elapsedSeconds).slice(-2);
      }

      // Guardar el identificador del intervalo
      intervalIds[spotIndex] = setInterval(updateTime, 1000);

      // Marcar el cajón como ocupado
      isOccupied[spotIndex] = true;
    }
  });
}


















// // Obtener todos los elementos con la clase "parking-spot"
// var parkingSpots = document.getElementsByClassName("parking-spot");
// var rate = 15; // tarifa por hora

// // Recorrer cada cajón de estacionamiento
// for (var i = 0; i < parkingSpots.length; i++) {
//   var spot = parkingSpots[i];
//   var isOccupied = false; // variable para indicar si el cajón está ocupado
//   var intervalId = null; // variable para guardar el identificador del intervalo
//   var elapsedSeconds = 0; // variable para guardar el tiempo transcurrido
//   var startTime = 0; // variable para guardar el tiempo de inicio

//   // Añadir un evento de clic a cada cajón de estacionamiento
//   spot.addEventListener("click", function () {
//     // Si el cajón ya está ocupado, salir de la función
//     if (isOccupied) {
//       return;
//     }

//     // Si el cajón ya está seleccionado, detener el temporizador y deseleccionarlo
//     if (this.classList.contains("selected")) {
//       clearInterval(intervalId);
//       this.classList.remove("selected");
//       return;
//     }

//     // Seleccionar el cajón de estacionamiento
//     this.classList.add("selected");

//     // Crear un elemento de temporizador y añadirlo al cajón
//     var timerElement = document.createElement("div");
//     timerElement.className = "timer";
//     timerElement.innerHTML = "00:00:00";
//     this.appendChild(timerElement);

//     // Empezar a contar el tiempo
//     startTime = new Date().getTime() - elapsedSeconds * 1000; // restar el tiempo transcurrido anteriormente

//     // Función para actualizar el tiempo restante
//     function updateTime() {
//       var currentTime = new Date().getTime();
//       elapsedSeconds = Math.floor((currentTime - startTime) / 1000);
//       var remainingTime = 2 - elapsedSeconds / 3600; // establecer la tarifa por hora
//       if (remainingTime <= 0) {
//         // Marcar el cajón como ocupado después de 2 horas
//         isOccupied = true;
//         clearInterval(intervalId);
//         spot.classList.remove("selected");
//         spot.classList.add("occupied");
//         spot.innerHTML = "Ocupado";
//         timerElement.remove(); // remover el temporizador del cajón
//         return;
//       }
//       spot.innerHTML = "$" + (remainingTime * rate).toFixed(2); // actualizar el texto del cajón
//       timerElement.innerHTML = pad(Math.floor(elapsedSeconds / 3600)) + ":" + pad(Math.floor(elapsedSeconds / 60) % 60) + ":" + pad(elapsedSeconds % 60); // actualizar el temporizador
//     }

//     // Empezar a actualizar el tiempo cada segundo
//     intervalId = setInterval(updateTime, 1000);
//   });
//   // Añadir un evento de doble clic para detener el temporizador y mostrar la tarifa a pagar
// spot.addEventListener("dblclick", function () {
//     clearInterval(intervalId);
//     isOccupied = true;
//     spot.classList.remove("selected");
//     spot.classList.add("occupied");
//     var currentTime = new Date().getTime();
//     elapsedSeconds = Math.floor((currentTime - startTime) / 1000);
//     var totalRate = Math.ceil((elapsedSeconds / 3600) * rate); // calcular la tarifa a pagar redondeando hacia arriba
//     spot.innerHTML = "$" + totalRate.toFixed(2); // mostrar la tarifa a pagar en el cajón
//     timerElement.remove(); // remover el temporizador del cajón
//     });
// }


// var parkingSpots = document.getElementsByClassName("parking-spot");

// // Crear variables para indicar si cada cajón está ocupado y guardar los identificadores de los intervalos
// var isOccupied = [];
// var intervalIds = [];

// // Recorrer cada cajón de estacionamiento
// for (var i = 0; i < parkingSpots.length; i++) {
//   // Establecer que el cajón no está ocupado y que no tiene un identificador de intervalo
//   isOccupied[i] = false;
//   intervalIds[i] = null;

//   // Añadir un evento de clic a cada cajón de estacionamiento
//   parkingSpots[i].addEventListener("click", function () {
//     // Obtener el índice del cajón que se ha hecho clic
//     var spotIndex = Array.prototype.indexOf.call(parkingSpots, this);
  
//     // Salir de la función si el cajón ya está ocupado
//     if (isOccupied[spotIndex]) {
//       return;
//     }

//     // Seleccionar el cajón de estacionamiento
//     this.classList.add("selected");

//     // Crear un elemento de temporizador y añadirlo al cajón
//     var timerElement = document.createElement("div");
//     timerElement.className = "timer";
//     timerElement.innerHTML = "00:00:00";
//     this.appendChild(timerElement);
  
//     // Empezar a contar el tiempo
//     var startTime = new Date().getTime();
    
//     // Función para actualizar el tiempo restante y el precio
//     function updateTime() {
//       var currentTime = new Date().getTime();
//       var elapsedTime = currentTime - startTime;
//       var elapsedHours = Math.floor(elapsedTime / 3600000);
//       var elapsedMinutes = Math.floor((elapsedTime % 3600000) / 60000);
//       var elapsedSeconds = Math.floor((elapsedTime % 60000) / 1000);
//       var remainingTime = 1; // establecer la tarifa por hora
//       if (elapsedHours >= 1) {
//         remainingTime += elapsedHours; // agregar horas extras
//       }
//       if (elapsedMinutes >= 1) {
//         remainingTime += 1; // agregar hora extra si ya pasó la hora
//       }

//       var price = remainingTime * 15; // calcular el precio

//       // Eliminar el temporizador anterior y el precio si ya existen
//       if (intervalIds[spotIndex] != null) {
//         clearInterval(intervalIds[spotIndex]);
//         intervalIds[spotIndex] = null;
//       }
//       var oldTimerElement = this.querySelector(".timer");
//       if (oldTimerElement != null) {
//         this.removeChild(oldTimerElement);
//       }
//       var oldPriceElement = this.querySelector(".price");
//       if (oldPriceElement != null) {
//         this.removeChild(oldPriceElement);
//       }

//       // Crear un elemento para el precio
//       var priceElement = document.createElement("div");
//       priceElement.className = "price";
//       priceElement.innerHTML = "$" + price.toFixed(2); // formatear el precio como dólares y centavos
//       this.appendChild(priceElement);

//       // Actualizar el temporizador
//       timerElement.innerHTML =
//         ("0" + elapsedHours).slice(-2) +
//         ":" +
//         ("0" + elapsedMinutes).slice(-2) +
//         ":" +
//         ("0" + elapsedSeconds).slice(-2);

//       // Guardar el identificador del intervalo
//       intervalIds[spotIndex] = setInterval(updateTime, 1000);
//     } });
//     // Añadir un evento de doble clic para finalizar el tiempo de estacionamiento
//     parkingSpots[i].addEventListener("dblclick", function () {
//         // Obtener el índice del cajón que se ha hecho clic
//         var spotIndex = Array.prototype.indexOf.call(parkingSpots, this);
    
//         // Si el cajón no está ocupado, salir de la función
//         if (!isOccupied[spotIndex]) {
//           return;
//         }
      
//         // Liberar el cajón de estacionamiento
//         this.classList.remove("selected");
//         isOccupied[spotIndex] = false;
      
//         // Detener el temporizador y eliminar el temporizador y el precio
//         clearInterval(intervalIds[spotIndex]);
//         intervalIds[spotIndex] = null;
//         var timerElement = this.querySelector(".timer");
//         if (timerElement != null) {
//           this.removeChild(timerElement);
//         }
//         var priceElement = this.querySelector(".price");
//         if (priceElement != null) {
//           this.removeChild(priceElement);
//         }
//       });
// }