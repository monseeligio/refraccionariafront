import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/services/renta/api.service';
import Swal from 'sweetalert2';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap'; 

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  @ViewChild("myModalInfo", {static: false}) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", {static: false}) myModalConf!: TemplateRef<any>; // inicialización con valor nulo
  mostrar=false;
  lado = 6;
  Datos={nombreProducto:'', marcaProducto:'', presentacionProducti:'', contenidoProducto:'', costoProducto:'' , proveedorProducto:'', cantidadIngresa:'', statusProducto:'', descripcionProducto:''}
  Datos3={id:'', nombreProducto:'', marcaProducto:'', presentacionProducti:'', contenidoProducto:'', costoProducto:'' , proveedorProducto:'', cantidadIngresa:'', statusProducto:'', descripcionProducto:''}

  id=''
  public datos2:any[]=[];
  public actu='';
  public nombre='';
  public marca='';
  public pre='';
  public con=0;
  public costo=0;
  public prov='';
  public cant=0;
  public status=false;
  public des='';


  constructor(private _serviceM:ApiService, private modalService: NgbModal) { }


  ngOnInit(): void {
    this.getProducto();
  }

  mostrarModalInfo(id:any){
    this.modalService.open(this.myModalInfo);
    console.log(id); 
    this.Datos3.id=id._id; 
    this.Datos3.nombreProducto=id.nombreProducto; 
    this.Datos3.marcaProducto=id.marcaProducto; 
    this.Datos3.presentacionProducti=id.presentacionProducti; 
    this.Datos3.contenidoProducto=id.contenidoProducto; 
    this.Datos3.costoProducto=id.costoProducto; 
    this.Datos3.proveedorProducto=id.proveedorProducto; 
    this.Datos3.cantidadIngresa=id.cantidadIngresa; 
    this.Datos3.statusProducto=id.statusProducto; 
    this.Datos3.descripcionProducto=id.descripcionProducto; 
  }

  mostrarModalConf(spot:any){
    this.modalService.open(this.myModalConf).result.then( r => {
    }, error => {
      console.log(error);

    });
  }
  agregarProduc(){  
    if(this.Datos.nombreProducto=='' || this.Datos.marcaProducto=='' || this.Datos.presentacionProducti=='' || this.Datos.contenidoProducto=='' || this.Datos.costoProducto=='' || this.Datos.proveedorProducto=='' || this.Datos.cantidadIngresa=='' || this.Datos.statusProducto=='' || this.Datos.descripcionProducto==''){
      window.alert("Debes ingresar todos los datos solicitados")
    }  
    else{
    this._serviceM.agregarProducto(this.Datos).subscribe((data:any)=>{
      console.log(this.Datos);
      Swal.fire({
        icon: 'success',
        title: 'Inserción exitosa',
        text: 'Se ha insertado el registro con éxito',
      });
      this.getProducto();
    })}
  }
  
  getProducto(){
    this._serviceM.consultaProducto().subscribe((data:any)=>{
      if (data['ok']){
        console.log(data);
        this.datos2=data['respuesta'];
        console.log(this.datos2);
      }
      else{
        console.log('error en la data');
        
      }
    })
  }

  Eliminar(id: string){    
    this._serviceM.eliminarProducto(id).subscribe((data:any)=>{
      console.log(this.id);
      Swal.fire({
        icon: 'success',
        title: 'Eliminación exitosa',
        text: 'Se elimino el registro',
      });
      this.getProducto();

    }) 
}

  
  Actualizar(id:string){ 
    if(this.Datos3.nombreProducto=='' || this.Datos3.marcaProducto=='' || this.Datos3.presentacionProducti=='' || this.Datos3.contenidoProducto=='' || this.Datos3.costoProducto=='' || this.Datos3.proveedorProducto=='' || this.Datos3.cantidadIngresa=='' || this.Datos3.statusProducto=='' || this.Datos3.descripcionProducto==''){
      window.alert("Debes ingresar todos los datos solicitados")
    }  
    else{   
    console.log(id);
    this._serviceM.actualizarProducto(id, this.Datos3).subscribe((data:any)=>{
      console.log(this.Datos3);
      Swal.fire({
        icon: 'success',
        title: 'Actualizacion exitosa',
        text: 'Se actualizo el registro',
      });
      this.getProducto();
      this.Datos3={id:'', nombreProducto:'', marcaProducto:'', presentacionProducti:'', contenidoProducto:'', costoProducto:'' , proveedorProducto:'', cantidadIngresa:'', statusProducto:'', descripcionProducto:''}

    })
  }
}

}
