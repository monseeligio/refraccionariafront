import { Injectable } from '@angular/core';
import {URL} from '../../config/config';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public url;
  constructor(private _http:HttpClient) {
    this.url=URL;
  } 

   //PERSONAL
  agregarUsuario(body:any){
    const url= `${this.url}/personal/nuevo`;
    return this._http.post(url,body);
  }

  consultaUsuario(){
    //variable propia del metodo
    const url= `${this.url}/obtener/personal`;
    return this._http.get(url);
  }

  eliminarUsuario(id:any){
    const url= `${this.url}/delete/Personal/${id}`;
    return this._http.delete(url);
  }

  actualizarUsuario(id:any, body:any){
    const url= `${this.url}/update/personal/${id}`;
    return this._http.put(url,body);
  }

  //PRODUCTOS
  agregarProducto(body:any){
    const url= `${this.url}/producto/nuevo`;
    return this._http.post(url,body);
  }
  
  consultaProducto(){
    const url= `${this.url}/obtener/producto`;
    return this._http.get(url);
  }
  eliminarProducto(id:any):Observable<any>{
    const url= `${this.url}/delete/Producto/${id}`;
    return this._http.delete(url);
  }
  actualizarProducto(id:any, body:any):Observable<any>{
    const url= `${this.url}/update/producto/${id}`;
    return this._http.put(url,body);
  }

  //PROVEEDOR

  consultaProveedor(){
    //variable propia del metodo
    const url= `${this.url}/obtener/proveedor`;
    return this._http.get(url);
  }

  //SUCURSAL
  consultasucursal(){
    //variable propia del metodo
    const url= `${this.url}/obtener/sucursal`;
    return this._http.get(url);
  }
  



}
